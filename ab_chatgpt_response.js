// 读取原始响应体
let body = $response.body;
let obj = JSON.parse(body);

// 修改 feature_gates 中的值
let featureGates = obj["feature_gates"];
for (const key in featureGates) {
    if (featureGates[key].value === false) {
        featureGates[key].value = true;
    }
}
obj["feature_gates"] = featureGates;

// 返回修改后的响应体
$done({body: JSON.stringify(obj)});
